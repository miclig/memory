package fr.umlv.android.memory;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A memory card
 */
public class Card implements Serializable
{
    private String path;
    private boolean visible = false;

    /** To cache all the drawables */
    private static Map<String, Drawable> drawableCache = new HashMap<>();

    public Card(String path)
    {
        this.path = path;
    }

    public Card clone()
    {
        return new Card(path);
    }

    public void setVisible(boolean value)
    {
        this.visible = value;
    }

    public boolean isVisible()
    {
        return this.visible;
    }

    public String getPath()
    {
        return this.path;
    }

    public boolean isSameCard(Card c)
    {
        return path.equals(c.path);
    }

    public static List<Card> loadFromAssets(Context context, String path) throws IOException
    {
        List<Card> cards = new ArrayList<>();
        String[] assets = context.getAssets().list(path);
        for (String asset: assets)
            cards.add(new Card(new File(path, asset).getPath()));
        return cards;
    }

    public static Drawable computeDrawable(Context context, String assetPath) throws IOException
    {
        InputStream is = context.getAssets().open(assetPath);
        Bitmap bitmap = ((BitmapDrawable)Drawable.createFromStream(is, null)).getBitmap();
        // Bitmap resized = Bitmap.createScaledBitmap(bitmap, 10, 10, false);
        return new BitmapDrawable(bitmap);
    }

    public Drawable getDrawable(Context context)
    {
        try {
            if (drawableCache.get(path) == null)
                drawableCache.put(path, computeDrawable(context, path));
        } catch (IOException e)
        {
            Log.e(getClass().getName(), "Cannot access the asset " + path);
        }
        return drawableCache.get(path);
    }

    public View getView(Context context, View v)
    {
        if (v == null) {
            v = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card, null);
        }
        ImageView iv = (ImageView)v.findViewById(R.id.image);
        iv.setBackground(context.getResources().getDrawable(visible?R.mipmap.card_front:R.mipmap.card_back, null));
        iv.setImageDrawable(visible ? getDrawable(context) : null);
        TextView tv = (TextView)v.findViewById(R.id.text);
        tv.setText(visible?path:"?");
        return v;
    }

    public String toString()
    {
        return path;
    }
}
