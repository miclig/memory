package fr.umlv.android.memory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Memory extends AppCompatActivity {
    private Card firstCard = null;
    private Card secondCard = null;
    private List<Card> cards;
    private int countFounds = 0;

    private boolean multitouchMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_memory);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            countFounds = savedInstanceState.getInt("countFounds");
            cards = (List<Card>)savedInstanceState.getSerializable("cards");
            int firstCardIndex = savedInstanceState.getInt("firstCardIndex", -1);
            int secondCardIndex = savedInstanceState.getInt("secondCardIndex", -1);
            firstCard = (firstCardIndex < 0)?null:cards.get(firstCardIndex);
            secondCard = (secondCardIndex < 0)?null:cards.get(secondCardIndex);
        } else {
            List<Card> cards = (List<Card>)getIntent().getSerializableExtra("cards");
            if (cards == null)
                try {
                    cards = Card.loadFromAssets(this, "cards");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            this.cards = new ArrayList<>();
            for (int i = 0; i < 2; i++)
                for (Card c: cards)
                    this.cards.add(c.clone());
            Collections.shuffle(cards);
        }

        final GridView grid = (GridView) findViewById(R.id.grid);
        //grid.setNumColumns(4);

        final GameAdapter adapter = new GameAdapter(this, this.cards);
        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (! multitouchMode)
                    check(position);
            }
        });
        // to implement multitouch cheat mode and drag'n'drop
        grid.setOnTouchListener(new View.OnTouchListener() {

            public static final long MOVE_DELAY = 1_000_000_000L; // in nanos

            List<Card> revealedCards = new ArrayList<Card>(); // revelead cards for cheat mode
            Card startCard = null;
            long actionDownTimestamp = -1;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int index = grid.pointToPosition((int)event.getX(event.getActionIndex()), (int)event.getY(event.getActionIndex()));
                Card c = (index >= 0)?cards.get(index):null;
                switch (event.getActionMasked())
                {
                    case MotionEvent.ACTION_DOWN:
                        if (c != null && ! c.isVisible())
                            revealedCards.add(c);
                        startCard = c;
                        actionDownTimestamp = System.nanoTime();
                        Log.i("LOG", "Action down " + revealedCards);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // start drag only if the pointer has been let down with no motion during MOVE_DELAY
                        if (! multitouchMode && actionDownTimestamp >= 0 && System.nanoTime() - actionDownTimestamp > MOVE_DELAY)
                        {
                            Log.i("LOG", "Drag");
                            return true;
                        } else
                            startCard = null;
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        multitouchMode = true;
                        if (revealedCards.size() > 0)
                            revealedCards.get(0).setVisible(true);
                        if (c != null && ! c.isVisible()) {
                            c.setVisible(true);
                            revealedCards.add(c);
                        }
                        Log.i("LOG", "Action pointer down " + revealedCards);
                        adapter.notifyDataSetChanged();
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.i("LOG", "Up: " + startCard + ", " + c);
                        actionDownTimestamp = -1;
                        if (multitouchMode) {
                            multitouchMode = false;
                            Log.i("LOG", "Action up:  " + revealedCards);
                            for (Card card : revealedCards)
                                card.setVisible(false);
                            revealedCards.clear();
                            adapter.notifyDataSetChanged();
                        } else if (startCard != null && c != null && ! startCard.equals(c))
                        {
                            Log.i("LOG", "Drop");
                            swap(startCard, c);
                            revealedCards.clear();
                            startCard = null;
                            adapter.notifyDataSetChanged();
                        }
                        break;
                }
                return false;
            }
        });
    }

    private void init()
    {
        countFounds = 0;
        for (Card c: cards)
            c.setVisible(false);
        firstCard = secondCard = null;
        ((GameAdapter)((GridView)findViewById(R.id.grid)).getAdapter()).notifyDataSetChanged();
    }

    public void restart(MenuItem mi)
    {
        init();
    }

    private void swap(Card c1, Card c2)
    {
        int i = cards.indexOf(c1);
        int j = cards.indexOf(c2);
        cards.set(i, c2);
        cards.set(j, c1);
    }


    private void check(int position) {
        if (secondCard != null) {
            firstCard.setVisible(false);
            secondCard.setVisible(false);
            firstCard = secondCard = null;
        }

        Card c = cards.get(position);

        if (! c.isVisible()) {
            c.setVisible(true);
            if (firstCard == null) {
                firstCard = c;
            } else {
                if (c.isSameCard(firstCard) && ! c.equals(firstCard)) {
                    countFounds += 2;
                    firstCard = secondCard = null;
                    if (countFounds == cards.size()) {
                        Toast.makeText(this, "FINISH !!!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    secondCard = c;
                }
            }
        }

        ((GameAdapter)((GridView)findViewById(R.id.grid)).getAdapter()).notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_memory, menu);
        return true;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("countFounds", countFounds);
        outState.putSerializable("cards", (Serializable) cards);
        outState.putInt("firstCardIndex", (firstCard == null)?-1:cards.indexOf(firstCard));
        outState.putInt("secondCard", (secondCard == null)?-1:cards.indexOf(secondCard));

        super.onSaveInstanceState(outState);
    }
}
